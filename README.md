# Intro
This challenge consists of letting users explore the world of Star Wars. We have made the challenge quite open ended to allow your creativity to run free and to explore some different designs, as creativity is an important skill we are looking for. You can use any library you want. This challenge uses the free Star Wars API, read about it here: https://swapi.co/ .

```
      _______              _______
     /\:::::/\            /\:::::/\
    /::\:::/::\          /==\:::/::\
   /::::\_/::::\   .--. /====\_/::::\
  /_____/ \_____\-' .-.`-----' \_____\
  \:::::\_/:::::/-. `-'.-----._/:::::/
   \::::/:\::::/   `--' \::::/:\::::/
    \::/:::\::/          \::/:::\::/    Sienar Fleet Systems' TIE/In
     \/:::::\/            \/:::::\/     Space Superiority Starfighter (1)
  LS  """""""              """""""
```
# Setup

Clone this repo by doing `git clone https://fictivr@bitbucket.org/fictivr/fictive-frontend-challenge.git` and run `npm install` in the folder. We've setup the repo with React but you are free to change to any library and frameworks you want.

# Goal

We want to be able to browse the Star Wars universe visually. We want to make this explorative, so the user shouldn't be presented with all data from start, but it should begin with one entity (Luke Skywalker, for example). The user should be presented with some relations/attributes that Luke has, to for example starships or planets, and when the user clicks them, he/she will be presented with all other entities that are connected to that. For example, Luke has flown an X-wing, and when we click the X-wing, we will also see other pilots of X-wings. This should be possible to continue as far as the API has data.

Our recommended way to visualize this is through a mind map/graph type structure. You can use a suitable library that draws it on a canvas or SVG, such as https://github.com/vasturiano/react-force-graph , but you are also free to use other ways to explore, such as a tree structure, as long as you always can visualize how something is connected back to your starting point (e.g. Luke).

As the user clicks and explores the data, make sure it stays consistent and clean. That means not showing the same person or starship in multiple places, or if you do that, at least make it clear that it's the same person as before (visually linked somehow). In other words, you shouldn't find two different Luke Skywalkers. You don't need to show all possible relations about an entity, focus on just one or a few, and maybe present some identifying information like name and type (that doesn't have to be clickable/explorable). The point is not to crowd the user with info.

# Bonus
If you want to add some polish, or show what you are more interested in or good at, you can for example:

1. Make a nice looking prototype / wireframe / design concept (it may of course include more than what you have had time to code)
2. Make the app look nicer (choice of color, typography, icons, elements, etc)
3. Polish the user experience e.g. things like working well on a touch screen
4. Work on performance and caching to minimise the amount of fetching required
5. Make it possible to filter or search the presented data somehow, to for example highlight only planets, or items containing a specific string.

# Submission
To submit your coding challenge, commit all your changes to the master branch and run the following command:
```
git bundle create fictive-frontend-challenge.bundle HEAD master
```
Email it to us at `martin.frojd@fictivereality.com`.

# What we will look for

* That user experience is good. Always better with less features or styling but at higher level of user experience, meaning how controls feel and behave.
* Readable code with good structure and comments where necessary. Use of modern Javascript and CSS features.
* Bug-free and tested
* A creative and/or playful solution and design.

Make sure to include a readme file that explains:
* How to run and use the app
* Reasoning around choices or trade-offs made. For example, if you didn't have time to do something, what were you planning to add, or where you see potential performance problems to solve in the future.